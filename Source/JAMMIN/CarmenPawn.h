// Fill out your copyright notice in the Description page of Project Settings.
// Author: Alex Hunt, alexmhunt3@gmail.com

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "CarmenPawn.generated.h"

UCLASS()
class JAMMIN_API ACarmenPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACarmenPawn();

	// holds the Mesh representing Carmen (the player)
	UPROPERTY(EditAnywhere)
	class UStaticMeshComponent * Mesh;

	UPROPERTY(BlueprintReadOnly)
	bool MapOn;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Component to help control Carmen's movement.
	UFloatingPawnMovement * PawnMovement;

	// A Camera that follows Carmen at an offset.
	UPROPERTY(EditAnywhere)
	class UCameraComponent * Camera;

	bool bisFlying;
	bool bPressedJump;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// handles input for moving forward and backward
	UFUNCTION()
	void MoveForward(float amount);

	// handles input for moving left and right
	UFUNCTION()
	void MoveRight(float amount);

	// turns map on and off
	UFUNCTION()
	void MapToggle();

	// handles jumping (perhaps add flight check here)
	UFUNCTION()
	void OnStartJump();

	UFUNCTION()
	void OnStopJump();

};
