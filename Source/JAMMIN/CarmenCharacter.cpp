// Fill out your copyright notice in the Description page of Project Settings.


#include "CarmenCharacter.h"
#include "Camera/CameraComponent.h" // needed due to forward declaration in header
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/EngineTypes.h"

#define notFlying (Movement->MovementMode != MOVE_Flying)

// Sets default values
ACarmenCharacter::ACarmenCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Instantiate components
	// Mesh does not need to be instantiated, as the Character class already does it for us.

	// Set up and instantiate the camera.
	/*Camera = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	Camera->SetRelativeLocation(FVector(-500.f,0.f,0.f)); // assigns offset, can be edited in UE4 editor
	Camera->SetupAttachment(GetMesh()); // tells camera to follow Carmen's mesh
	*/

	// Create a camera boom (pulls in towards the player if the camera collides with something)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 200.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = true; // Rotate camera relative to arm, necessary for vertical flight control

	// Set up character movement component.
	Movement = GetCharacterMovement(); // already created in Character constructor

	//Map is turned off when created
	MapOn = false;
}

// Called when the game starts or when spawned
void ACarmenCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACarmenCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void ACarmenCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// To see all keybinds, open the project in UE4 and go to "Edit->Project Settings->Input".
	// Set up walking movement bindings.
	PlayerInputComponent->BindAxis("MoveForward", this, &ACarmenCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACarmenCharacter::MoveRight);

	// Set up jump bindings.
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACarmenCharacter::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACarmenCharacter::OnStopJump);
	
	// Toggle flight mode (F)
	PlayerInputComponent->BindAction("ToggleFlight", IE_Pressed, this, &ACarmenCharacter::ToggleFlight);

	// Set up "look" bindings.
	// Allows player to turn or look up/down using the mouse.
	PlayerInputComponent->BindAxis("Turn", this, &ACarmenCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &ACarmenCharacter::AddControllerPitchInput);

	//Set up Map Key binding
	PlayerInputComponent->BindAction("MapToggle", IE_Pressed, this, &ACarmenCharacter::MapToggle );

	
}

// Handles forward and backward movement.
// If amount is positive, player moves forward, or backward if negative.
void ACarmenCharacter::MoveForward(float amount){
	if(notFlying){ // WASD controls disabled during flight.
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, amount); // Input component takes care of the rest!
	}
}

// Handles left and right movement.
// If amount is positive, player moves right, or left if negative.
void ACarmenCharacter::MoveRight(float amount){
	if(notFlying){ // WASD controls disabled during flight.
	// Find out which way is "right" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, amount); // Input component takes care of the rest!
	}
}

// Handles jumping upward. If space is pressed again while jumping, flight mode activates.
void ACarmenCharacter::OnStartJump(){
	if (notFlying){
		// make player jump up, handled by the CharacterMovementComponent
		bPressedJump = true;
		UE_LOG(LogTemp, Warning, TEXT("JUMP START"));
	}
	
	
}

// Handles ending a jump (when the space bar is released.)
void ACarmenCharacter::OnStopJump(){
	bPressedJump = false;
	UE_LOG(LogTemp, Warning, TEXT("JUMP STOP"));
}

void ACarmenCharacter::ToggleFlight(){
	if(notFlying)
		OnFlightModeStart();
	else
		OnFlightModeStop();
}

// Handles entering flight mode
// TODO: Implement actual flight movement.
void ACarmenCharacter::OnFlightModeStart(){
	bPressedJump = false;
	
	UE_LOG(LogTemp, Warning, TEXT("FLIGHT MODE START"));
	// Set player movement mode to Flying.
	Movement->SetMovementMode(MOVE_Flying);
	
}

// TODO: Implement Flight Mode ending when player lands on the ground.
void ACarmenCharacter::OnFlightModeStop(){
	

	UE_LOG(LogTemp, Warning, TEXT("FLIGHT MODE STOP"));
	// Set player movement mode back to Walking.
	Movement->SetMovementMode(MOVE_Walking); 
}


//Switches value of MapOn
void ACarmenCharacter::MapToggle(){
	MapOn = !MapOn;
	if(MapOn == true){
	UE_LOG(LogTemp, Warning, TEXT("MapToggle True"));
	}
	else{
	UE_LOG(LogTemp, Warning, TEXT("MapToggle False"));
	}

}