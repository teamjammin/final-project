// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CarmenCharacter.generated.h"

UCLASS()
class JAMMIN_API ACarmenCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACarmenCharacter();

	//Boolean for opening and closing the map
	UPROPERTY(BlueprintReadOnly)
	bool MapOn;

	// holds the Mesh representing Carmen (the player)
	// uses SkeletalMesh rather than StaticMesh beceause
	// skeletal meshes allow for animation.
	//UPROPERTY(EditAnywhere)
	//class USkeletalMeshComponent * Mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Camera boom positioning the camera behind the character
	// Taken from UE4's Third Person example
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	// Follow camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(EditAnywhere)
	class UCharacterMovementComponent * Movement;
		

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// handles input for moving forward and backward
	UFUNCTION()
	void MoveForward(float amount);

	// handles input for moving left and right
	UFUNCTION()
	void MoveRight(float amount);

	// handles input for jumping
	UFUNCTION()
	void OnStartJump();

	UFUNCTION()
	void OnStopJump();

	// calls the appropriate flight start/stop function depending on input
	// true = start flying, false = stop flying
	// true by default
	UFUNCTION()
	void ToggleFlight();

	// handles entering flight mode
	UFUNCTION()
	void OnFlightModeStart();

	// handles exiting flight mode
	UFUNCTION()
	void OnFlightModeStop();

	// turns map on and off
	UFUNCTION()
	void MapToggle();
	

};
