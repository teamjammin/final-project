#include "Agent.h"

AAgent::AAgent(){
	PrimaryActorTick.bCanEverTick = true;
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("AgentMesh"));	
	RootComponent = Mesh;
	Velocity = FMath::VRand();
	Velocity = Velocity.GetSafeNormal();
	Velocity = Velocity * FMath::RandRange(10,20);

}

void AAgent::BeginPlay(){
	Super::BeginPlay();
}

void AAgent::Init( USkeletalMesh *mesh, int id ) {
	UE_LOG(LogTemp, Warning, TEXT("Agent initialized.") );
	SetActorRelativeScale3D(FVector(1,1,1));
	location = GetActorLocation();
	idnum = id;
}

void AAgent::Tick(float DeltaTime){
	Super::Tick(DeltaTime);
	FRotator angle = Velocity.Rotation();
	FRotator angle2 = FRotator(angle.Pitch,angle.Yaw-90, angle.Roll);
	SetActorRotation(angle2, ETeleportType::None);
	const FVector loc = GetActorLocation();
	SetActorLocation( loc + Velocity );
	
}

FVector AAgent::GetLoc(){
	return location;
}