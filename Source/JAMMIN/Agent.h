#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Agent.generated.h"

UCLASS()
class JAMMIN_API AAgent : public AActor
{
	GENERATED_BODY()
	
public:	
	AAgent();
	void Init( USkeletalMesh *mesh, int id );

	UPROPERTY(EditAnywhere,Category="Map")
	class USkeletalMeshComponent * Mesh;

	UPROPERTY(EditAnywhere)
	FVector Velocity;

	FVector GetLoc();
	int idnum;

protected:
	virtual void BeginPlay() override;
	FVector location;

public:	
	virtual void Tick(float DeltaTime) override;
};