// Fill out your copyright notice in the Description page of Project Settings.
// Author: Alex Hunt, alexmhunt3@gmail.com

#include "CarmenPawn.h"
#include "Camera/CameraComponent.h" // needed due to forward declaration in header

// Sets default values
ACarmenPawn::ACarmenPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Instantiate components
	PawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>("PawnMovement");
	
	// Instantiate mesh
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");

	// Set up and instantiate the camera.
	Camera = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	Camera->SetRelativeLocation(FVector(-500.f,0.f,0.f)); // assigns offset, can be edited in UE4 editor
	Camera->SetupAttachment(Mesh); // tells camera to follow Carmen's mesh

	MapOn = true;
	bisFlying = false;
	bPressedJump = false;
}

// Called when the game starts or when spawned
void ACarmenPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACarmenPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACarmenPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up walking movement bindings
	// to see all keybinds, open the project in UE4 and go to "Edit->Project Settings->Input"
	PlayerInputComponent->BindAxis("MoveForward", this, &ACarmenPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACarmenPawn::MoveRight);
	PlayerInputComponent->BindAction("MapToggle", IE_Pressed, this, &ACarmenPawn::MapToggle );


	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACarmenPawn::OnStartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACarmenPawn::OnStopJump);
}

// Handles forward and backward movement.
// If amount is positive, player moves forward, or backward if negative.
void ACarmenPawn::MoveForward(float amount){
	// hand over the direction of movement as a vector to the Movement component
	PawnMovement->AddInputVector(GetActorForwardVector() * amount);
}

// Handles left and right movement.
// If amount is positive, player moves right, or left if negative.
void ACarmenPawn::MoveRight(float amount){
	// hand over the direction of movement as a vector to the Movement component
	PawnMovement->AddInputVector(GetActorRightVector() * amount);
}

void ACarmenPawn::MapToggle(){
	MapOn = !MapOn;
	if(MapOn == true){
	UE_LOG(LogTemp, Warning, TEXT("MapToggle True"));
	}
	else{
	UE_LOG(LogTemp, Warning, TEXT("MapToggle False"));
	}

}
// Handles jumping upward. If space is pressed again while jumping, flight mode activates.
void ACarmenPawn::OnStartJump(){
	if (!bisFlying){
		// make player jump up
		bPressedJump = true;
	}
	UE_LOG(LogTemp, Warning, TEXT("JUMP START"));
}

void ACarmenPawn::OnStopJump(){
	bPressedJump = false;
	UE_LOG(LogTemp, Warning, TEXT("JUMP STOP"));
}