#include "FlockingManager.h"
#include "Agent.h"

#define AGENT_COUNT 10

void UFlockingManager::Init( UWorld *world, USkeletalMesh *mesh, TSubclassOf<class AAgent> agentType ) {
    
    World = world;
    float incr = (PI * 2.f) / AGENT_COUNT;
    for( int i = 0; i < AGENT_COUNT; i++ ) {
        if( World != nullptr ) {
            FRotator rotation = FRotator();

            //lay boids in a ring
            FVector location = FVector::ZeroVector; 
            location.X = FMath::Sin( incr * i ) * 600.f+ 5000;
            location.Y = FMath::Cos( incr * i ) * 600.f;
            location.Z= 500;

            FActorSpawnParameters SpawnParams;
            AAgent * agent = World->SpawnActor<AAgent>( agentType, location, rotation, SpawnParams  );
            agent->Init( mesh, i ); //init with mesh and id number
            Agents.Add( agent );
        }
    }

    initialized = true;
    
}



void UFlockingManager::Flock() {
    for(int32 i =0 ; i< Agents.Num(); ++i){
        AAgent* Boid = Agents[i];
        FVector R1 = RuleOne(Boid);
        FVector R2 = RuleTwo(Boid);
        FVector R3 = RuleThree(Boid);
        FVector R4 = Bound(Boid);

        FVector NewVelocity = Boid->Velocity;
        R1.Normalize();
        R2.Normalize();
        R3.Normalize();
        NewVelocity = NewVelocity + (((R1)+ (R2*50) +(R3/10))/60) + (R4/200) ;
        float max = 15;
        float min = 10;
        const float speed = FMath::Clamp(NewVelocity.Size(), min, max );
        NewVelocity.Normalize();
        NewVelocity = NewVelocity * speed ;
        float vx =NewVelocity.X;
        float vy =NewVelocity.Y;
        float vz = NewVelocity.Z;
        Boid->Velocity.Set(vx,vy,vz);

    }

}



FVector UFlockingManager::RuleOne(AAgent* CurrBoid){ //towards center of other boids
    FVector Center = FVector::ZeroVector; 
    for(int32 j =0 ; j< Agents.Num(); ++j){
            if (Agents[j]->idnum != CurrBoid->idnum){
                Center = Center + Agents[j]->GetActorLocation();
            }
    }
    Center = Center / (AGENT_COUNT-1);
   return ((Center - CurrBoid->GetActorLocation()));

}


FVector UFlockingManager::RuleTwo(AAgent* CurrBoid){ //separation
    FVector C = FVector(0.f);
    for(int i =0 ; i< AGENT_COUNT; i++){
        if (Agents[i] != CurrBoid ){
            FVector loc1 = Agents[i]->GetActorLocation();
            FVector loc2 = CurrBoid->GetActorLocation();
            float dist = FVector::Dist(loc1, loc2);
            dist = abs(dist);
            if(dist < 300){
                C = C - (loc1-loc2);
                UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), dist);
            }
        }

    }
    return (C/ (AGENT_COUNT-1));

}

FVector UFlockingManager::RuleThree(AAgent* CurrBoid){ //Align
    FVector Center = FVector(0.f);
    for(int i =0 ; i< AGENT_COUNT; i++){
        if (Agents[i] != CurrBoid){
            Center = Center + Agents[i]->Velocity;
        }
    }

    Center = Center / (AGENT_COUNT-1);
    return ((Center - (CurrBoid->Velocity)));
}

FVector UFlockingManager::Bound (AAgent* CurrBoid){
    int Xmin =-5000;
    int Xmax =5000;
    int Ymin =-5000;
    int Ymax =5000;
    int Zmin =2000;
    int Zmax =5000;
    FVector v = FVector(0.f);

    if (CurrBoid->GetActorLocation().X<Xmin){
        v.X =10;
    }
    else if (CurrBoid->GetActorLocation().X>Xmax){
        v.X = -10;
    }
    if (CurrBoid->GetActorLocation().Y<Ymin){
        v.Y =10;
    }
    else if (CurrBoid->GetActorLocation().Y>Ymax){
        v.Y = -10;
    }    
    if (CurrBoid->GetActorLocation().Z<Zmin){
        v.Z =10;
    }
    else if (CurrBoid->GetActorLocation().Z>Zmax){
        v.Z = -10;
    }
    return v;

}

