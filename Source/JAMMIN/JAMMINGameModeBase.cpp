// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "JAMMINGameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "Components/StaticMeshComponent.h"


AJAMMINGameModeBase::AJAMMINGameModeBase(){
    PlayerMesh = CreateDefaultSubobject<USkeletalMesh>(TEXT("PlayerMesh"));
    PrimaryActorTick.bCanEverTick = true; //tick method should be called on every frame
}

void AJAMMINGameModeBase::BeginPlay() {
    Super::BeginPlay();

    PersistantWidget = CreateWidget<UUserWidget>(GetWorld(), PersistWidgetClass);
    PersistantWidget->AddToViewport();

    Manager = NewObject<UFlockingManager>(); //create new manager
    Manager->Init( GetWorld(), PlayerMesh, Agent );
    UE_LOG(LogTemp, Warning, TEXT("Base Begin Play"));
}

void AJAMMINGameModeBase::Tick( float DeltaTime ) {
    Super::Tick( DeltaTime );
    Manager->Flock();

};