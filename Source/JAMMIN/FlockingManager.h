#pragma once

#include "CoreMinimal.h"
#include "FlockingManager.generated.h"

UCLASS()
class JAMMIN_API UFlockingManager : public UObject
{

public:
	GENERATED_BODY()

	void Init( UWorld *world, USkeletalMesh *mesh, TSubclassOf<class AAgent> agentType );
	void Flock(); 
	UPROPERTY(EditAnywhere)
	class AActor* Goal;

private:
	UWorld *World;	
	UPROPERTY(EditAnywhere)
	bool initialized;
	UPROPERTY(EditAnywhere)
	TArray<class AAgent *> Agents; //array stores all agents
	FVector RuleOne(AAgent* CurrBoid);
	FVector RuleTwo(AAgent* CurrBoid);
	FVector RuleThree(AAgent* CurrBoid);
	FVector Bound (AAgent* CurrBoid);


};