// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FlockingManager.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "JAMMINGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JAMMIN_API AJAMMINGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	AJAMMINGameModeBase();

	public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Map")
	TSubclassOf<class UUserWidget> HUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Map")
	TSubclassOf<class UUserWidget> PersistWidgetClass;


	UPROPERTY(EditAnywhere, Category="Map")
    TSubclassOf<class AAgent> Agent;

	UPROPERTY(EditAnywhere, Category="Map")
    class USkeletalMesh *PlayerMesh;

	
	UPROPERTY(EditAnywhere, Category="Map")
	class UFlockingManager *Manager;
	
	UPROPERTY()
	class UUserWidget * PersistantWidget;

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
};
