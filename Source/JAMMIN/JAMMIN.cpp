// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "JAMMIN.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, JAMMIN, "JAMMIN" );
